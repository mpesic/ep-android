package com.pesic.miha.epstore;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.HashMap;
import java.util.Map;

public class UserInfo extends AppCompatActivity {
    public User user;
    private static final String TAG = EnterScreen.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        final EditText editName = (EditText) findViewById(R.id.name_edit);
        final EditText editSurname = (EditText) findViewById(R.id.surname_edit);
        final EditText editEmail = (EditText) findViewById(R.id.email_edit);
        final EditText editAddress = (EditText) findViewById(R.id.address_edit);
        final EditText editPhone = (EditText) findViewById(R.id.phone_edit);

        Button buttEditProfile = (Button) findViewById(R.id.editUser_button);
        Button buttStartShopping = (Button) findViewById(R.id.startShopping_button);

        Gson gs = new Gson();
        //try {
            String target = getIntent().getStringExtra("user");
            user = gs.fromJson(target, User.class);

            String name = user.getName();
            String surname = user.getSurname();
            String email = user.getEmail();
            String address = user.getAddress();
            String phone = user.getTelephone();

            editName.setText(name);
            editSurname.setText(surname);
            editEmail.setText(email);
            editAddress.setText(address);
            editPhone.setText(phone);
        //} catch (Exception e) {}

        final RequestQueue queue = Volley.newRequestQueue(this);

        HttpsTrustManager.allowAllSSL();

        buttStartShopping.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    @SuppressWarnings("rawtypes")
                    Class browse = Class.forName("com.pesic.miha.epstore.BrowseItems");
                    Intent intent = new Intent(UserInfo.this, BrowseItems.class);
                    startActivity(intent);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        buttEditProfile.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                String id = user.getId();
                String USER = "https://api.ep-store.local/v1/users/" + id;

                final StringRequest stringRequest = new StringRequest(Request.Method.PUT, USER,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                Toast.makeText(UserInfo.this, "Update successful.", Toast.LENGTH_LONG).show();
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(UserInfo.this, "Invalid username or password.", Toast.LENGTH_LONG).show();
                        Log.w(TAG, "Exception: " + error.getLocalizedMessage());
                        finish();
                        startActivity(getIntent());
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        String username_pass = ((Globals) getApplication()).getUsername();
                        String password_pass = ((Globals) getApplication()).getPassword();
                        return EnterScreen.createBasicAuthHeader(editEmail.getText().toString(), password_pass);
                    }
                    @Override
                    protected Map<String, String> getParams() throws AuthFailureError {
                        final Map<String, String> params = new HashMap<>();
                        params.put("name", editName.getText().toString().trim());
                        params.put("surname", editSurname.getText().toString().trim());
                        params.put("address", editAddress.getText().toString().trim());
                        params.put("telephone", editPhone.getText().toString().trim());
                        // params.put("email", editEmail.getText().toString().trim());
                        return params;
                    }
                };


                queue.add(stringRequest);
            }
        });
    }

    public void handleResponse(String response) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_user_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_orderHistory:
                goOrderHistory();
                return true;

            case R.id.action_viewCart:
                // goViewCart();
                return true;

            case R.id.action_browseItems:
                goBrowse();
                return true;

            case R.id.action_checkout:
                // goCheckout();
                return true;

            case R.id.action_log_out:
                goLogout();
                return true;

            default:
                return super.onOptionsItemSelected(item);

        }
    }

    public void goBrowse() {
        Intent intent = new Intent(UserInfo.this, BrowseItems.class);
        startActivity(intent);
    }

    public void goOrderHistory() {
        Intent intent = new Intent(UserInfo.this, ShowOrders.class);
        startActivity(intent);
    }

    public void goViewProfile() {
        Intent intent = new Intent(UserInfo.this, UserInfo.class);
        startActivity(intent);
    }
    /*
    public void goViewCart() {
        Intent intent = new Intent(UserInfo.this, Cart.class);
        startActivity(intent);
    }

    public void goCheckout() {
        Intent intent = new Intent(UserInfo.this, Checkout.class);
        startActivity(intent);
    }*/

    public void goLogout() {
        Intent intent = new Intent(UserInfo.this, EnterScreen.class);
        intent.putExtra("logout","yes");
        startActivity(intent);
    }



}
