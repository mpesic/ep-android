package com.pesic.miha.epstore;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.NetworkImageView;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.Locale;
import java.util.Map;

public class ShowDetails extends AppCompatActivity {

    private static final String TAG = ShowDetails.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_details);

        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);


        Gson gs = new Gson();

        String target = getIntent().getStringExtra("item");
        StoreItem item = gs.fromJson(target, StoreItem.class);


        String display = item.getDisplay();
        String description = item.getDescription();
        double price = item.getPrice();
        double rating = item.getRating();
        String imageURL = item.getDisplay_image_id();


        TextView idN = (TextView) findViewById(R.id.textview_id);
        TextView descN = (TextView) findViewById(R.id.textview_description);
        TextView priceN = (TextView) findViewById(R.id.textview_price);
        TextView ratN = (TextView) findViewById(R.id.textview_rating);


        idN.setText(display);
        descN.setText("Description: \n" + description);
        priceN.setText(String.format(Locale.ENGLISH, "Price: %.2f EUR ", price));
        ratN.setText("Rating: " + Double.toString(rating));

        NetworkImageView nv = (NetworkImageView) findViewById(R.id.imView_item);
        nv.setDefaultImageResId(R.drawable.sample_image);

        if(imageURL != null) {
            // imageURL = "http://api.ep-store.local/item_assets/6/images/e11963531bdce0a9c485b29bcf1ed36e";
            nv.setImageUrl(imageURL, MySingleton.getInstance(this).getImageLoader());
        }



    }

}
