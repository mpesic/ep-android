package com.pesic.miha.epstore;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.google.gson.Gson;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.util.HashMap;
import java.util.Map;

public class EnterScreen extends AppCompatActivity {

    String name = "Test User";
    String pass = "testpassword123";

    public static final String USER = "https://api.ep-store.local/v1/users/me";
    private static final String TAG = EnterScreen.class.getCanonicalName();

    public EditText textUser = null;
    public EditText textPass = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_screen);

        textUser = (EditText) findViewById(R.id.userName_text);
        textPass = (EditText) findViewById(R.id.pass_text);
        Button buttLogin = (Button) findViewById(R.id.login_button);
        Button buttGuest = (Button) findViewById(R.id.guest_button);


        try {
            Intent intent = getIntent();
            String logout = intent.getStringExtra("logout");
            if(logout.equals("yes")) {
                textUser.setText("");
                textPass.setText("");
            }
        } catch(Exception e) {
            // we do nothing
        }

        final RequestQueue queue = Volley.newRequestQueue(this);

        HttpsTrustManager.allowAllSSL();

        buttGuest.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                try {
                    @SuppressWarnings("rawtypes")
                    Class browse = Class.forName("com.pesic.miha.epstore.BrowseItems");
                    Intent intent = new Intent(EnterScreen.this, BrowseItems.class);
                    startActivity(intent);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });

        buttLogin.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                ((Globals) getApplication()).setUsername(textUser.getText().toString());
                ((Globals) getApplication()).setPassword(textPass.getText().toString());

                final StringRequest stringRequest = new StringRequest(Request.Method.GET, USER,
                        new Response.Listener<String>() {
                            @Override
                            public void onResponse(String response) {
                                handleResponse(response);
                            }
                        }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(EnterScreen.this, "Invalid username or password.", Toast.LENGTH_LONG).show();
                        Log.w(TAG, "Exception: " + error.getLocalizedMessage());
                        finish();
                        startActivity(getIntent());
                    }
                }) {
                    @Override
                    public Map<String, String> getHeaders() throws AuthFailureError {
                        return createBasicAuthHeader(textUser.getText().toString(), textPass.getText().toString());
                    }
                };

                queue.add(stringRequest);
            }
        });

    }

    private void handleResponse(String response) {
        final Gson gson = new Gson();
        try {
            final User user = gson.fromJson(response, User.class);
            // textUser.setText(user.getName());

            getToUserProfile(user);
        }
        catch (Exception e) {
            textUser.setText("No Correcto, Sir!");
        }
    }

    public void getToUserProfile(User user) {
        Intent intent = new Intent(EnterScreen.this, UserInfo.class);
        Gson gs = new Gson();
        String target = gs.toJson(user);
        intent.putExtra("user", target);
        startActivity(intent);
    }


    public static Map<String, String> createBasicAuthHeader(String username, String password) {
        Map<String, String> headerMap = new HashMap<String, String>();

        String credentials = username + ":" + password;
        String base64EncodedCredentials =
                Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);
        headerMap.put("Authorization", "Basic " + base64EncodedCredentials);

        return headerMap;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_enter_screen, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
