package com.pesic.miha.epstore;

import android.app.Application;

public class Globals extends Application {

    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String someVariable) {
        this.username = someVariable;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String someVariable) {
        this.password = someVariable;
    }
}
