package com.pesic.miha.epstore;


import java.util.Locale;

public class StoreItem {

    public String id;
    public String display;
    public String description;
    public double price;
    public boolean active;
    public String vendor_id;
    public String created_at;
    public String updated_at;
    public String display_image_url;
    public double num_ratings;
    public double average_rating;
    public double num_orders;
    public String[] images_urls;


    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "%s \n%.2f EUR \nRating: %.1f", display, price, average_rating);
    }

    public String[] getImages_urls() {return this.images_urls;}

    public String getDisplay() {return this.display;}

    public String getDescription() {return this.description;}

    public double getPrice() {return this.price;}

    public double getRating() {return this.average_rating;}

    public String getDisplay_image_id() {return  this.display_image_url;}

}



/*

{"id":"2","display":"autem","description":"Dolore dicta qui rem.","price":20,"active":true,"vendor_id":"2",
"created_at":"2016-01-10 15:48:41",
"updated_at":"2016-01-10 15:48:41","display_image_id":null,"num_ratings":0,"average_rating":null,"num_orders":2}

 */

