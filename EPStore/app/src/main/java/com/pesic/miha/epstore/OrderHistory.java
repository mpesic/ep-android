package com.pesic.miha.epstore;

import android.app.ActionBar;
import android.app.ListActivity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import java.util.Map;

public class OrderHistory extends ListActivity {

    public static final String ALL_ORDERS = "https://api.ep-store.local/v1/orders";
    private static final String TAG = OrderHistory.class.getCanonicalName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
/*
        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#085B9E")));

        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));*/
/*
        HttpsTrustManager.allowAllSSL();



        final RequestQueue queue = Volley.newRequestQueue(this);
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, ALL_ORDERS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        handleResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(OrderHistory.this, "An error occurred.", Toast.LENGTH_LONG).show();
                Log.w(TAG, "Exception: " + error.getLocalizedMessage());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                String username_pass = ((Globals) getApplication()).getUsername();
                String password_pass = ((Globals) getApplication()).getPassword();
                return EnterScreen.createBasicAuthHeader(username_pass, password_pass);
            }
        };

        queue.add(stringRequest);
*/
    }

    private void handleResponse(String response) {
        final Gson gson = new Gson();
        final Order[] orders = gson.fromJson(response, Order[].class);

        final ArrayAdapter<Order> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, orders);

        //final ArrayAdapter<StoreItem> adapter = new ArrayAdapter<>(this,
        //R.layout.my_list_layout, items);

        setListAdapter(adapter);
    }

}
