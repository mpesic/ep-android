package com.pesic.miha.epstore;


public class User {

    /*
    {"id":"7","name":"Karl","surname":"Belin","email":"karl.belin@store.local","address":null,
    "telephone":null,"active":true,"permission_level_id":"3","created_by":null,
    "created_at":"2016-01-13 12:49:27","updated_at":"2016-01-13 12:49:27"}
     */

    public String id;
    public String name;
    public String surname;
    public String email;
    public String address;
    public String telephone;
    public boolean active;
    public String permission_level_id;
    public String created_by;
    public String created_at;
    public String updated_at;

    public String getId() {return this.id;}
    public String getName() {return this.name;}
    public String getSurname() {return  this.surname;}
    public String getEmail() {return  this.email;}
    public String getAddress() {return  this.address;}
    public String getTelephone() {return  this.telephone;}

}
