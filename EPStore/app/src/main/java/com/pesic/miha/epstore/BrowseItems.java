package com.pesic.miha.epstore;

import android.app.ActionBar;
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.gson.Gson;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import android.widget.Toast;
import android.util.Log;


public class BrowseItems extends ListActivity{

    public static final String ALL_ITEMS = "http://api.ep-store.local/v1/items";
    private static final String TAG = BrowseItems.class.getCanonicalName();
    /*

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_browse_items);

        Item[] items = Item.populateItems(20);

        // ListView lv = (ListView) findViewById(android.R.id.list);
        ListView lv = getListView();

        ArrayAdapter<Item> adapter = new ArrayAdapter<Item>(this,
                android.R.layout.simple_list_item_1, items);
        lv.setAdapter(adapter);
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        // Item item = (Item) getListAdapter().getItem(position);
        Item item = (Item) l.getAdapter().getItem(position);
        Intent intent = new Intent(BrowseItems.this, ShowDetails.class);
        Gson gs = new Gson();
        String target = gs.toJson(item);
        intent.putExtra("item", target);
        startActivity(intent);
    }
    */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ActionBar bar = getActionBar();
        bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#085B9E")));

        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(this.getResources().getColor(R.color.colorPrimaryDark));

        final RequestQueue queue = Volley.newRequestQueue(this);
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, ALL_ITEMS,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        handleResponse(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(BrowseItems.this, "An error occurred.", Toast.LENGTH_LONG).show();
                Log.w(TAG, "Exception: " + error.getLocalizedMessage());
            }
        });

        queue.add(stringRequest);
    }

    private void handleResponse(String response) {
        final Gson gson = new Gson();
        final StoreItem[] items = gson.fromJson(response, StoreItem[].class);

        final ArrayAdapter<StoreItem> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, items);

        //final ArrayAdapter<StoreItem> adapter = new ArrayAdapter<>(this,
                //R.layout.my_list_layout, items);

        setListAdapter(adapter);
    }


    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        // Item item = (Item) getListAdapter().getItem(position);
        StoreItem item = (StoreItem) l.getAdapter().getItem(position);
        Intent intent = new Intent(BrowseItems.this, ShowDetails.class);
        Gson gs = new Gson();
        String target = gs.toJson(item);
        intent.putExtra("item", target);
        startActivity(intent);
    }


}
