package com.pesic.miha.epstore;

import java.text.DecimalFormat;
import java.util.Locale;
import java.util.Random;

public class Item {


    private String id;
    private String description;
    public double price;
    private boolean available;
    private double rating;

    public Item(String i, String des, double pr, boolean av, double rt) {
        this.id = i;
        this.description = des;
        this.price = pr;
        this.available = av;
        this.rating = rt;
    }

    public static Item[] populateItems(int n) {
        Item[] items = new Item[n];
        Random random = new Random();
        // DecimalFormat df = new DecimalFormat("#.##");

        for(int i = 0; i < n; i++) {
            String idNew = "Item " + (String) Integer.toString(i+1);
            String deNew = "Description " + (String) Integer.toString(i+1);
            double prNew = random.nextDouble() * 1000;
            // prNew = Double.valueOf(df.format(prNew));
            double rtNew = random.nextInt(5) + 1;
            boolean avNew = random.nextBoolean();

            Item it = new Item(idNew, deNew, prNew, avNew, rtNew);
            items[i] = it;
        }
        return items;
    }

    @Override
    public String toString() {

        return String.format(Locale.ENGLISH, "%s \n%.2f EUR \nRating: %.1f", id, price, rating);
    }

    public String getDescription() {
        return this.description;
    }

    public String getId() {
        return this.id;
    }

    public boolean getAvailable() {
        return this.available;
    }

    public double getRating() {
        return this.rating;
    }

    public double getPrice() {
        return this.price;
    }

}
