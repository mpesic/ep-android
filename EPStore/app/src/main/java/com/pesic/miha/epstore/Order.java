package com.pesic.miha.epstore;


import java.util.Locale;

public class Order {

    /*
    [{"id":"18","quantity":1,"status":1,"order_group_id":"9","item_id":"6",
    "created_at":"2016-01-19 17:30:47","updated_at":"2016-01-19 17:30:47"}]
     */

    String id;
    int quantity;
    String status;
    String order_group_id;
    String item_id;
    String created_at;
    String updated_at;

    @Override
    public String toString() {
        return String.format(Locale.ENGLISH, "Item id: %s \nQuantity: %d \nCreated at: %s", item_id,
                quantity, created_at);
    }

}
